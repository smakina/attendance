package interfaces;

import model.User;

public interface UserSelectListener {
    void select(User user);
}
