package controller;

import db.DbHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.util.Callback;
import model.GType;

import javax.swing.text.View;
import java.net.URL;
import java.util.ResourceBundle;

public class DialogGypes implements Initializable {
    private DbHelper dbHelper;

    @FXML
    private TextField name;

    @FXML
    private Button saveBtn;

    @FXML
    private ListView<GType> listView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dbHelper = new DbHelper();

        saveBtn.setOnAction(event -> {
            GType gType = new GType("", name.getText());
            dbHelper.createGType(gType);
            listView.getItems().add(0, gType);
            name.setText("");
        });

        setData();
    }

    private void setData(){

        listView.setCellFactory(param -> new ListCell<GType>(){
            @Override
            protected void updateItem(GType item, boolean empty) {
                super.updateItem(item, empty);

                if (empty) {
                    setText(null);
                    setOnMouseClicked(null);
                }
                else {
                    setText(item.getName());
                    setOnMouseClicked(event -> {
                        if (event.getClickCount() == 2) {
//                            label.setText(item.getFirstName() + item.getLastName());
                        }
                    });
                }
            }
        });

        listView.getItems().addAll(dbHelper.getGTypes());
    }



}
