package controller;

import db.DbHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import model.Gadget;

import java.net.URL;
import java.util.ResourceBundle;

public class DialogNewGadget implements Initializable {

    private DbHelper dbHelper;

    @FXML
    private TextField srNo;
    @FXML
    private TextField name;
    @FXML
    private TextField type;
    @FXML
    private Button saveBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dbHelper = new DbHelper();

        saveBtn.setOnAction(event -> {
            saveData();
        });
    }

    private void saveData(){
        Gadget gadget = new Gadget("", "", "", srNo.getText(), name.getText(), type.getText());
        if( dbHelper.createGadget(gadget) ){
//            srNo.setText("");
//            name.setText("");
//            type.setText("");
        }
    }


}
