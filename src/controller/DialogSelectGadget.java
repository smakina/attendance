package controller;

import handler.GadgetListHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import model.Gadget;

import java.net.URL;
import java.util.ResourceBundle;

public class DialogSelectGadget implements Initializable {
    @FXML
    private TableView<Gadget> gadgetTableView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        GadgetListHandler gl = new GadgetListHandler(gadgetTableView);
        gl.setData();
    }

}
