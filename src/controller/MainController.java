package controller;

import handler.GadgetListHandler;
import handler.NewUserHandler;
import handler.SelectedUserHandler;
import interfaces.UserSelectListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import model.Gadget;
import model.User;
import table_helper.TableController;

import java.net.URL;
import java.util.ResourceBundle;

import static dialog.Dialog.*;

public class MainController implements Initializable {

    public static MainController mainController;
    public static UserSelectListener userSelectListener;

    public TextField et_name;
    public TextField et_department;
    public RadioButton rd_staff;
    public RadioButton rd_student;
    public RadioButton rd_visitor;
    public TextArea et_visit_reason;
    public Button btn_create_user;
    public Button btn_reset;
    public Button new_gadget;

    public TableController tableController;

    public Label lb_name;
    public Label lb_department;
    public Label lb_type;

    @FXML
    TableView<User> tableView;
    public TableView<Gadget> gadgetTableView;
    public TableView<Gadget> userGadgets;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mainController = this;
        tableController = new TableController(tableView);
        new NewUserHandler(this);
        GadgetListHandler allGadgetsHandler = new GadgetListHandler(gadgetTableView);
        allGadgetsHandler.setData();

        GadgetListHandler myGadgetsHandler = new GadgetListHandler(userGadgets);
        initComponents();
    }

    private void initComponents(){
        new_gadget.setOnAction(event -> {
            newGadgetDialog(event);
//            dialogList(event);
//            newGadgetType(event);
        });

        userSelectListener = user -> {
            new SelectedUserHandler(user);
        };
    }


}
