package handler;

import db.DbHelper;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Gadget;
import java.util.List;


public class GadgetListHandler {
    private TableView<Gadget> tableView;
    private List<Gadget> gadgetList;
    private DbHelper dbHelper;

    public GadgetListHandler(TableView<Gadget> gadgetTableView){
        this.tableView = gadgetTableView;
        dbHelper = new DbHelper();
        setColumns();
    }

    public void setData(){
        loadTableData(getData());
    }

    private void setColumns(){
        // Add columns to the TableView
        tableView.getColumns().addAll(
                getTH("#", "id"),
                getTH("Serial Number", "srNo"),
                getTH("Name", "name"),
                getTH("type", "type"),
                getTH("controls", "controls")
        );

        // Set the column resize policy to constrained resize policy
        tableView.setColumnResizePolicy(javafx.scene.control.TableView.CONSTRAINED_RESIZE_POLICY);
        // Set the Placeholder for an empty table
        tableView.setPlaceholder(new Label(""));
        tableView.setId("table");
    }

    public static TableColumn<Gadget, Integer> getTH(String name, String ref){
        TableColumn<Gadget, Integer> idCol = new TableColumn<>(name);
        PropertyValueFactory<Gadget, Integer> idCellValueFactory = new PropertyValueFactory<>(ref);
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }

//    private List<User> getCountArrayList(String jsonString){
//        System.out.println(jsonString);
//        List<Gadget> list = new ArrayList<>();
//        JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
//        JsonArray jsonElements = jsonObject.getAsJsonArray("data");
//
//        for (JsonElement je : jsonElements ) {
//            JsonObject jb = je.getAsJsonObject();
//
//            list.add(new User(
//                    "",
//                    ""+jb.get("User_name").getAsString(),
//                    ""+jb.get("status").getAsString(),
//                    ""+jb.get("total_files").getAsInt()
//            ));
//        }
//
//        return list;
//    }

//    public void setCountArrayList(List<User> userList) {
//        this.userList = userList;
//    }

//    public void addUserToTable(User user){
//        userList.add(0, user);
//        loadTableData();
//    }

    private List<Gadget> getData(){
        return dbHelper.gadgetList();
    }

    private void loadTableData(List<Gadget> gadgetList){
        tableView.getItems().clear();
        tableView.getItems().addAll(gadgetList);
        tableView.refresh();
    }
}
