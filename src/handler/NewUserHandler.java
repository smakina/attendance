package handler;

import controller.MainController;
import db.DbHelper;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import model.User;

public class NewUserHandler {

    private DbHelper dbHelper;
    public TextField et_name;
    public TextField et_department;
    public RadioButton rd_staff;
    public RadioButton rd_student;
    public RadioButton rd_visitor;
    public TextArea et_visit_reason;
    public Button btn_create_user;
    public Button btn_reset;

    private MainController mainController;

    private String type = "Staff";

    public NewUserHandler(MainController mainController) {
        this.mainController = mainController;
        dbHelper = new DbHelper();
        initComponents();
    }

    private void initComponents(){
        et_name = mainController.et_name;
        et_department = mainController.et_department;
        rd_staff = mainController.rd_staff;
        rd_student = mainController.rd_student;
        rd_visitor = mainController.rd_visitor;
        et_visit_reason = mainController.et_visit_reason;
        btn_create_user = mainController.btn_create_user;
        btn_reset = mainController.btn_reset;

        setListeners();
    }

    private void setListeners(){
        setRadioButton(rd_staff, rd_student, rd_visitor); //
        btn_create_user.setOnAction(event -> {
            User user = saveUser();
            if ( user != null ){
                mainController.tableController.addUserToTable(user);
            }else {
                // Alert
            }
        });

        btn_reset.setOnAction(event -> {
            et_name.setText("");
            et_department.setText("");
//            et_visit_reason.setText();
        });
    }

    private void setRadioButton(RadioButton...rbs){
        for (RadioButton rb : rbs){
            rb.setOnAction(event -> {
                type = rb.getText();
                for (RadioButton rb1 : rbs){
                    rb1.setSelected(rb1.equals(rb));
                }
            });
        }
    }

    private User saveUser(){
        if (et_name.getText().equals("") || et_department.getText().equals("")){
            return null;
        }
        User user = new User("",et_name.getText(), type, et_department.getText());

        if (dbHelper.createUser(user)){
           return user;
        }
        return null;
    }


}
