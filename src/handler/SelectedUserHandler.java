package handler;

import controller.MainController;
import main.Main;
import model.User;

public class SelectedUserHandler {
    private User user;
    private MainController mainController;

    public SelectedUserHandler(User user) {
        this.user = user;
        this.mainController = MainController.mainController;
        setData();
    }

    private void setData() {
        mainController.lb_name.setText(user.getName());
        mainController.lb_type.setText(user.getType());
        mainController.lb_department.setText(user.getDepartment());
    }

}
