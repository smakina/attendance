package model;

public class Visit {
    private String id;
    private String userId;
    private String date;
    private String time;
    private String visitReason;

    public Visit(String id, String userId, String date, String time, String visitReason) {
        this.id = id;
        this.userId = userId;
        this.date = date;
        this.time = time;
        this.visitReason = visitReason;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getVisitReason() {
        return visitReason;
    }
}
