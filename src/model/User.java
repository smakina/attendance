package model;

import controller.MainController;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class User {
    private String id;
    private String name;
    private String type;
    private String department;
    private Visit lastVisit;
    private String visitReason;
    private HBox controls;

    public User(String id, String name, String type, String department) {
        this.name = name;
        this.type = type;
        this.department = department;
    }

    public void setLastVisit(Visit lastVisit) {
        this.lastVisit = lastVisit;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getDepartment() {
        return department;
    }

    public Visit getLastVisitModel() {
        return lastVisit;
    }

    public String getLastVisit(){
        if (lastVisit == null)
            return "N/A";
        return String.format("%s %s", lastVisit.getDate(), lastVisit.getTime());
    }

    public String getVisitReason(){
        if (lastVisit == null)
            return "N/A";
        return lastVisit.getVisitReason();
    }

    public HBox getControls() {
        HBox hBox = new HBox();
        hBox.setSpacing(10);
        Button checkIn = new Button("check in");
        checkIn.setOnAction(event -> {
            MainController.userSelectListener.select(this);
        });

        Button checkOut = new Button("check out");
        Button view = new Button("View");
        hBox.getChildren().addAll(checkIn, checkOut, view);
        return hBox;
    }
}
