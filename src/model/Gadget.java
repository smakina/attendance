package model;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;

import java.util.List;

public class Gadget {

    private String id;
    private String uid;
    private String srNo;
    private String name;
    private String type;
    private String manufacture;
    private List<String> controls;

    public Gadget(String id, String uid, String srNo, String name, String manufacture,  String type) {
        this.id = id;
        this.uid = uid;
        this.srNo = srNo;
        this.name = name;
        this.type = type;
        this.manufacture = manufacture;
    }

    public String getId() {
        return id;
    }

    public String getSrNo() {
        return srNo;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getManufacture() {
        return manufacture;
    }

    public HBox getControls() {
        HBox hBox = new HBox();
        CheckBox select = new CheckBox("select");
        hBox.getChildren().addAll(select);
        return hBox;
    }
}
