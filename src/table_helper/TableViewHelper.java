package table_helper;

import javafx.collections.FXCollections;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import model.User;

import javax.jws.soap.SOAPBinding;
import java.util.List;

public class TableViewHelper {

    // Returns an observable list of persons
    public static List<User> getUserList()    {
        User p1 = new User("1","Salvation","100", "Admin");
        User p2 = new User("2", "Salvation","100", "Admin");
        User p3 = new User("3","Salvation","100", "Admin");
        User p4 = new User("4", "Salvation","100", "Admin");
        User p5 = new User("5","Salvation","100", "Admin");
        User p6 = new User("6","Salvation","100", "Admin");

        return FXCollections.observableArrayList(p1, p2, p3, p4, p5, p6);
    }
    private String name;
    private String type;
    private String department;

    public static TableColumn<User, Integer> getFName(){
        TableColumn<User, Integer> idCol = new TableColumn<>("Name");
        PropertyValueFactory<User, Integer> idCellValueFactory = new PropertyValueFactory<>("name");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }

    public static TableColumn<User, Integer> getType(){
        TableColumn<User, Integer> idCol = new TableColumn<>("Type");
        PropertyValueFactory<User, Integer> idCellValueFactory = new PropertyValueFactory<>("type");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }

    public static TableColumn<User, Integer> getDepartment(){
        TableColumn<User, Integer> idCol = new TableColumn<>("Department");
        PropertyValueFactory<User, Integer> idCellValueFactory = new PropertyValueFactory<>("department");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }

    public static TableColumn<User, Integer> getLastVisit(){
        TableColumn<User, Integer> idCol = new TableColumn<>("Last Visit Date");
        PropertyValueFactory<User, Integer> idCellValueFactory = new PropertyValueFactory<>("lastVisit");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }

    public static TableColumn<User, Integer> getVisitReason(){
        TableColumn<User, Integer> idCol = new TableColumn<>("Visit Reason");
        PropertyValueFactory<User, Integer> idCellValueFactory = new PropertyValueFactory<>("visitReason");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }

    public static TableColumn<User, Integer> getTLastUpdated() {
        TableColumn<User, Integer> idCol = new TableColumn<>("Last Updated");
        PropertyValueFactory<User, Integer> idCellValueFactory = new PropertyValueFactory<>("lastUpdated");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }

    public static TableColumn<User, Integer> getControls(){
        TableColumn<User, Integer> idCol = new TableColumn<>("");
        PropertyValueFactory<User, Integer> idCellValueFactory = new PropertyValueFactory<>("controls");
        idCol.setCellValueFactory(idCellValueFactory);
        return idCol;
    }
}
