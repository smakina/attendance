package table_helper;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import db.DbHelper;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import model.User;

import java.util.ArrayList;
import java.util.List;

public class TableController {
    private DbHelper dbHelper;
    public static TableView<User> tableView;
//    public static DbHelper dbHelper;
    private List<User> userList;
    private String date;

    public TableController(TableView<User> tableView){
        TableController.tableView = tableView;
        dbHelper = new DbHelper();
        setColumns();
        userList = dbHelper.getUserList();
        loadTableData();
    }

    private void setColumns(){
        // Add columns to the TableView
        tableView.getColumns().addAll(
                TableViewHelper.getFName(),
                TableViewHelper.getType(),
                TableViewHelper.getDepartment(),
                TableViewHelper.getLastVisit(),
                TableViewHelper.getVisitReason(),
                TableViewHelper.getControls()
        );

        // Set the column resize policy to constrained resize policy
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        // Set the Placeholder for an empty table
        tableView.setPlaceholder(new Label(""));
        tableView.setId("countTable");
    }

    private List<User> getCountArrayList(String jsonString){
        System.out.println(jsonString);
        List<User> list = new ArrayList<>();
        JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
        JsonArray jsonElements = jsonObject.getAsJsonArray("data");

        for (JsonElement je : jsonElements ) {
            JsonObject jb = je.getAsJsonObject();

            list.add(new User(
                    "",
                    ""+jb.get("User_name").getAsString(),
                    ""+jb.get("status").getAsString(),
                    ""+jb.get("total_files").getAsInt()
            ));
        }

        return list;
    }

    public void setCountArrayList(List<User> userList) {
        this.userList = userList;
    }

    public void addUserToTable(User user){
        userList.add(0, user);
        loadTableData();
    }


    private void loadTableData(){
        tableView.getItems().clear();
        tableView.getItems().addAll(userList);
        tableView.refresh();
    }

}
