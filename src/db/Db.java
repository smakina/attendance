package db;

import config.*;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Db {
    public static Connection connection;
    private DbHelper dbHelper;

    public Db(){
        File file = new File(Config.DB_PATH);

        if (!file.exists()){
            file.mkdirs();
        }

        initConnection();
    }

    private void initConnection(){
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:"+Config.DB_PATH+Config.DB_NAME);
            dbHelper = new DbHelper();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static Connection getConnection(){
        if ( connection == null)
            new Db();
        return connection;
    }

    public DbHelper getDbHelper() {
        return dbHelper;
    }
}