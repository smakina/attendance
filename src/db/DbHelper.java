package db;

import model.GType;
import model.Gadget;
import model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DbHelper {
    private String tableName = "counts";

    public DbHelper() {
        createTables();
    }

    public boolean createUser(User user) {
        String query = "INSERT INTO users(" +
                "name, department, type)" +
                "VALUES('" +
                user.getName() + "','" +
                user.getType() + "','" +
                user.getDepartment() + "')";

        try {
            PreparedStatement pr = Db.getConnection().prepareStatement(query);
            pr.executeUpdate();
            pr.close();
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public boolean createGadget(Gadget gadget) {
        String query = "INSERT INTO gadgets(" +
                "srNo, name, type)" +
                "VALUES('" +
                gadget.getSrNo() + "','" +
                gadget.getName() + "','" +
                gadget.getType() + "')";

        try {
            PreparedStatement pr = Db.getConnection().prepareStatement(query);
            pr.executeUpdate();
            pr.close();
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public boolean createGType(GType gType) {
        String query = "INSERT INTO gTypes( name )" +
                "VALUES('" +gType.getName() + "')";

        try {
            PreparedStatement pr = Db.getConnection().prepareStatement(query);
            pr.executeUpdate();
            pr.close();
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public boolean insertCount() {
//        String query = "INSERT INTO counts(" +
//                "totalPages, totalFiles, totalCorrupt, path)" +
//                "VALUES('" +
//                count.getTotalPages().replace(",", "") + "','" +
//                count.getTotalFiles().replace(",", "") + "','" +
//                count.getTotalCorrupt().replace(",", "") + "','" +
//                count.getPath() + "'" +
//                ")";
//
//        try {
//            PreparedStatement pr = Db.connection.prepareStatement(query);
//            pr.executeUpdate();
//            pr.close();
//            return true;
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }

        return false;
    }


    private void createTables() {

        String query =
                "CREATE TABLE IF NOT EXISTS users(" +
                        "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "name TEXT," +
                        "department TEXT," +
                        "type TEXT," +
                        "timeStamp DATETIME DEFAULT CURRENT_TIMESTAMP," +
                        "path TEXT)";
        executeQuery(query);

        query =
                "CREATE TABLE IF NOT EXISTS gadgets(" +
                        "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "uid TEXT," +
                        "srNo TEXT," +
                        "name TEXT," +
                        "type TEXT," +
                        "manufacture TEXT," +
                        "timeStamp DATETIME DEFAULT CURRENT_TIMESTAMP )";

        executeQuery(query);

        query =
                "CREATE TABLE IF NOT EXISTS gTypes(" +
                        "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "name TEXT," +
                        "timeStamp DATETIME DEFAULT CURRENT_TIMESTAMP )";

        executeQuery(query);
    }


    private void executeQuery(String query) {
        PreparedStatement pr;
        try {
            pr = Db.getConnection().prepareStatement(query);
            pr.execute();

            System.out.println("Table Created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean deleteColumn(int id) {
        String query = "DELETE FROM counts WHERE id='" + id + "'";
        try {
            PreparedStatement pr = Db.connection.prepareStatement(query);
            pr.executeUpdate();
            pr.close();
            System.out.println("record deleted " + id);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public List<User> getUserList() {
        List<User> counts = new ArrayList<>();

        String query = "SELECT * FROM users ORDER BY id DESC";
        try {
            PreparedStatement pr = Db.connection.prepareStatement(query);

            ResultSet resultSet = pr.executeQuery();

            while (resultSet.next()) {

                counts.add(new User(
                        resultSet.getString("id"),
                        resultSet.getString("name"),
                        resultSet.getString("department"),
                        resultSet. getString("type")
                ));

            }

            resultSet.close();
            pr.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return counts;
    }

    public List<Gadget> gadgetList() {
        List<Gadget> counts = new ArrayList<>();

        String query = "SELECT * FROM gadgets ORDER BY id DESC";
        try {
            PreparedStatement pr = Db.connection.prepareStatement(query);

            ResultSet resultSet = pr.executeQuery();

            while (resultSet.next()) {

                counts.add(new Gadget(
                        ""+resultSet.getString("id"),
                        ""+resultSet.getString("uid"),
                        ""+resultSet.getString("srNo"),
                        ""+resultSet.getString("name"),
                        ""+resultSet.getString("manufacture"),
                        ""+resultSet. getString("type")
                ));

            }

            resultSet.close();
            pr.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return counts;
    }

    public List<GType> getGTypes() {
        List<GType> gTypes = new ArrayList<>();

        String query = "SELECT * FROM gTypes ORDER BY id DESC";
        try {
            PreparedStatement pr = Db.connection.prepareStatement(query);

            ResultSet resultSet = pr.executeQuery();

            while (resultSet.next()) {

                gTypes.add(new GType(
                        resultSet.getString("id"),
                        resultSet.getString("name")
                ));

            }

            resultSet.close();
            pr.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return gTypes;
    }

//    public ArrayList<Count> getCounts() {
//        ArrayList<Count> counts = new ArrayList<>();
//
//        String query = "SELECT * FROM counts ORDER BY id DESC";
//        try {
//            PreparedStatement pr = Db.connection.prepareStatement(query);
//
//            ResultSet resultSet = pr.executeQuery();
//
//            while (resultSet.next()) {
//
//                counts.add(new Count(
//                        resultSet.getInt("id"),
//                        resultSet.getInt("totalPages"),
//                        resultSet.getInt("totalFiles"),
//                        resultSet.getInt("totalCorrupt"),
//                        resultSet.getString("timeStamp"),
//                        resultSet.getString("path")
//                ));
//
//            }
//
//            resultSet.close();
//            pr.close();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return counts;
//    }
}
